#ifndef __APP_AUDIO_H__
#define __APP_AUDIO_H__

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>

#include <mpg123.h>
#include <ao/ao.h>
#include <alsa/asoundlib.h>
#include <alsa/mixer.h>
#include <alloca.h> // needed for mixer

#include <thread>
#include <sched.h>

#include "ak.h"

#include "app.h"
#include "app_data.h"
#include "app_dbg.h"

#include "task_list.h"

#include "sys_dbg.h"

#define APP_AUDIO_DEBUG_EN
#ifdef APP_AUDIO_DEBUG_EN
#define AUDIO_DBG(fmt, ...)		__LOG__(fmt, "AUDIO_DBG", ##__VA_ARGS__)
#else
#define AUDIO_DBG(fmt, ...)
#endif


#define HW_CARD_TABLE_LEN				4

#define	THREAD_PLAYER_STATE_UNINT		0
#define	THREAD_PLAYER_STATE_IDLE		1
#define	THREAD_PLAYER_STATE_PLAYING		2
#define	THREAD_PLAYER_STATE_STOP		3

using namespace std;

typedef struct {
	string nonConvertCard;
	string ConvertCard;
	string MonononConvertCard;
	string MonoConvertCard;
} hw_card_t;

extern hw_card_t hw_card_table[HW_CARD_TABLE_LEN];

class app_audio {
public:
	typedef enum {
		AUDIO_TYPE_NONE,
		AUDIO_TYPE_MP3,
		AUDIO_TYPE_WAV,
	} audio_type_e;

	app_audio() {
		memset(this, 0, sizeof(app_audio));

		m_pthread_player_state = THREAD_PLAYER_STATE_UNINT;

		m_playing_volume = (double)0.00;
		m_playing_song_name = NULL;

		m_is_called_play = false;
	}

	~app_audio() {
		stop();
	}

	app_audio(const char* dev_name) {
		memset(this, 0, sizeof(app_audio));

		m_hw_id = get_hw_id(dev_name);

		m_ao_option.key = (char*)"dev";
		m_ao_option.value = (char*)dev_name;

		m_pthread_player_state = THREAD_PLAYER_STATE_UNINT;

		m_playing_volume = (double)0.00;
		m_playing_song_name = NULL;
	}

	static void deinitialize() {
		mpg123_exit();
		ao_shutdown();
	}

	static void initialize() {
		ao_initialize();
		mpg123_init();

		int result;
		mpg123_pars *mp;
		mp = mpg123_new_pars(&result);

		if (mp == NULL) {
			FATAL("app_audio", 0x10);
		}

		/* https://sourceforge.net/p/mpg123/mailman/message/19154183/ */
		if (MPG123_OK == mpg123_par(mp, MPG123_RESYNC_LIMIT, 0, 0)) {
			AUDIO_DBG("MPG123_RESYNC_LIMIT OK !\n");
		}
		else {
			AUDIO_DBG("MPG123_RESYNC_LIMIT ERR !\n");
		}
	}

	void start() {
		m_ao_driver_id = ao_driver_id("alsa");

		m_pthread_player_state = THREAD_PLAYER_STATE_IDLE;
	}

	void start(const char* dev_name) {
		m_ao_driver_id = ao_driver_id("alsa");

		m_hw_id = get_hw_id(dev_name);

		m_ao_option.key = (char*)"dev";
		m_ao_option.value = (char*)dev_name;

		m_pthread_player_state = THREAD_PLAYER_STATE_IDLE;
	}

	audio_type_e get_audio_type(const char* song);

	int play(const char* song, double volume);
	int play(const char* song);
	int play_mp3(const char* song);
	int play_wav(const char* song);
	void stop();
	int set_volume(double volume);
	double get_volume();

	int is_called_play();

private:
	// Audio members
	void play_stop();

	static void* thread_mp3_player_entry(void* me) {
		((app_audio*)me)->mp3_player_handler();
		return NULL;
	}

	static void* thread_wav_player_entry(void* me) {
		((app_audio*)me)->wav_player_handler();
		return NULL;
	}

	void mp3_player_handler();
	void wav_player_handler();

	void set_pthread_player_state(int);
	int get_pthread_player_state();

	int get_hw_id(const char* dev_name);

	int get_wav_song_format(const char* song, ao_sample_format* sample_format, uint32_t* data_size);
	void alsa_set_volume(float volume);

	// Audio thread membersormat
	pthread_t		m_pthread_player;
	pthread_mutex_t	m_pthread_mutex_player;
	int m_pthread_player_state;
	audio_type_e m_audio_playing_type;

	double m_playing_volume;
	string* m_playing_song_name;

	mpg123_handle* m_mpg123_handle;
	ao_option m_ao_option;
	ao_sample_format m_ao_sample_format;
	ao_device* m_ao_device;

	FILE* m_wav_pfile;
	int m_ao_driver_id;

	unsigned char* m_stream_buffer;
	size_t m_stream_buffer_size;
	size_t m_stream_done;

	int m_info_channels;
	int m_info_encoding;
	long m_info_rate;

	int m_error;
	bool m_is_called_play;

	int m_hw_id;
};

#endif //__APP_AUDIO_H__
