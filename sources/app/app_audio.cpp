#include "app_audio.h"


struct sched_param  thread_sched_param;

//typedef struct {
//	const char* nonConvertCard;
//	const char* ConvertCard;
//	const char* MonononConvertCard;
//	const char* MonoConvertCard;
//} hw_card_t;

//hw_card_t hw_card_table[HW_CARD_TABLE_LEN] = {
//	{	"NonConvertSet",	"Set",		"Mono_NonConvertSet",	"Mono_Set"		},
//	{	"NonConvertSet_1",	"Set_1",	"Mono_NonConvertSet_1",	"Mono_Set_1"	},
//	{	"NonConvertSet_2",	"Set_2",	"Mono_NonConvertSet_2",	"Mono_Set_2"	},
//	{	"NonConvertSet_3",	"Set_3",	"Mono_NonConvertSet_3",	"Mono_Set_3"	}
//};

hw_card_t hw_card_table[4] = {
	{	"NonConvertAudio",		"Audio",	"Mono_NonConvertAudio",		"Mono_Audio"	},
	{	"NonConvertAudio_1",	"Audio_1",	"Mono_NonConvertAudio_1",	"Mono_Audio_1"	},
	{	"NonConvertAudio_2",	"Audio_2",	"Mono_NonConvertAudio_2",	"Mono_Audio_2"	},
	{	"NonConvertAudio_3",	"Audio_3",	"Mono_NonConvertAudio_3",	"Mono_Audio_3"	}
};

int app_audio::get_hw_id(const char *dev_name) {
	int hw_id = -1;

	for (int i = 0; i < HW_CARD_TABLE_LEN; i++) {
		if ( (hw_card_table[i].ConvertCard.compare(string(dev_name)) == 0) ||
			 (hw_card_table[i].nonConvertCard.compare(string(dev_name)) == 0) ||
			 (hw_card_table[i].MonoConvertCard.compare(string(dev_name)) == 0) ||
			 (hw_card_table[i].MonononConvertCard.compare(string(dev_name)) == 0) ) {
			hw_id = i;
			break;
		}
	}

	return hw_id;
}

int app_audio::get_wav_song_format(const char *song, ao_sample_format *sample_format, uint32_t* data_size) {
	/*
	 *  wave file header format:
	 *      (4 byte) type = "RIFF"
	 *      (4 byte) size (data, not file)
	 *      (4 byte) type = "WAVE"
	 *      (4 byte) type = "fmt "
	 *
	 *      (32 bit) chunk_size
	 *      (16 bit) format_type = 1 (1 = PCM, uncompressed)
	 *      (16 bit) number_of_channels (1 = mono, 2 = stereo, can go on to several channels)
	 *      (32 bit) sample_rate (samples per second)
	 *      (32 bit) average_bytes_per_second
	 *      (16 bit) bytes_per_sample
	 *      (16 bit) bits_per_sample (bytes_per_sample * 8) (quality of sound, 8 bit, 16 bit)
	 *
	 *      (4 byte) data = "data"
	 *      (32 bit) size (size of data)
	 *
	 */

	FILE *file_pointer = NULL;

	// http://www.cplusplus.com/forum/general/205408/
	file_pointer = fopen(song, "rb"); // binary mode

	if (!file_pointer) {
		AUDIO_DBG("Error: missing / bad file\n");
		return -1;
	}

	// declare variables
	char type_1[5] = { 0 }; // "RIFF"
	char type_2[5] = { 0 }; // "WAVE"
	char type_3[5] = { 0 }; // "fmt "
	uint32_t size, chunk_size;
	short format_type, channels;
	uint32_t sample_rate, avg_bytes_per_sec;
	short bytes_per_sample, bits_per_sample;
	char type_4[5] = { 0 }; // "data"
	size_t st_size;

	(void)st_size;

	// check for RIFF format
	st_size = fread(type_1, sizeof(char), 4, file_pointer);
	if (strcmp(type_1, "RIFF")) {
		AUDIO_DBG("WRN: not RIFF format\n");
	}

	// data size
	st_size = fread(&size, sizeof(uint32_t), 1, file_pointer);

	// check for WAVE format
	st_size = fread(type_2, sizeof(char), 4, file_pointer);
	if (strcmp(type_2, "WAVE")) {
		AUDIO_DBG("WRN: not WAVE format\n");
	}

	// check for "fmt " string
	st_size = fread(type_3, sizeof(char), 4, file_pointer);
	if (strcmp(type_3, "fmt ")) {
		AUDIO_DBG("WRN: not fmt format\n");
	}

	// chunk size
	st_size = fread(&chunk_size, sizeof(uint32_t), 1, file_pointer);

	// format type
	st_size = fread(&format_type, sizeof(short), 1, file_pointer);

	// number of channels
	st_size = fread(&channels, sizeof(short), 1, file_pointer);

	// sample rate
	st_size = fread(&sample_rate, sizeof(uint32_t), 1, file_pointer);

	// average bytes per second
	st_size = fread(&avg_bytes_per_sec, sizeof(uint32_t), 1, file_pointer);

	// bytes per sample
	st_size = fread(&bytes_per_sample, sizeof(short), 1, file_pointer);

	// bits per sample
	st_size = fread(&bits_per_sample, sizeof(short), 1, file_pointer);

	// check for "data" string
	st_size = fread(type_4, sizeof(char), 4, file_pointer);

	if (strcmp(type_4, "data")) {
		AUDIO_DBG("Error: missing data string\n");
	}

	// data size
	uint32_t st_data_size = 0;
	st_size = fread(&st_data_size, sizeof(uint32_t), 1, file_pointer);
	*data_size = st_data_size;

	ao_sample_format st_ao_sample_format;
	st_ao_sample_format.bits = bits_per_sample;
	st_ao_sample_format.rate = sample_rate;
	st_ao_sample_format.channels = channels;
	st_ao_sample_format.byte_format = format_type;
	st_ao_sample_format.matrix = 0;

	memcpy(sample_format, &st_ao_sample_format, sizeof(ao_sample_format));

	fclose(file_pointer);
	return 1;
}

// Function copied from:
// http://stackoverflow.com/questions/6787318/set-alsa-master-volume-from-c-code
// Written by user "trenki".
// cat /proc/asound/cards
// amixer -c 2 scontents
void app_audio::alsa_set_volume(float volume) {
	AUDIO_DBG("alsa_set_volume: %0.2f\n", volume);
	long min, max;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;
	const char *card = "Device";
	const char *selem_name = "Speaker";

	snd_mixer_open(&handle, 0);
	snd_mixer_attach(handle, card);
	snd_mixer_selem_register(handle, NULL, NULL);
	snd_mixer_load(handle);

	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, selem_name);
	snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);

	snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	AUDIO_DBG("snd_mixer_selem_get_playback_volume_range: min %2.2f\n", (float) min);
	AUDIO_DBG("snd_mixer_selem_get_playback_volume_range: max %2.2f\n", (float) max);
	snd_mixer_selem_set_playback_volume_all(elem, volume * max / 100);
	AUDIO_DBG("snd_mixer_selem_set_playback_volume_all: val %0.2f\n", volume * max / 100.0);

	snd_mixer_close(handle);
}

app_audio::audio_type_e app_audio::get_audio_type(const char* song) {
	int song_name_len = static_cast<string>(song).length();
	AUDIO_DBG("song_name_len: %d\n", song_name_len);

	/* check MP3 file */
	if (song_name_len > 4 \
			&&  song[song_name_len - 1] == '3'\
			&& (song[song_name_len - 2] == 'p' || song[song_name_len - 2] == 'P') \
			&& (song[song_name_len - 3] == 'm' || song[song_name_len - 3] == 'M') \
			&&  song[song_name_len - 4] == '.') {
		return AUDIO_TYPE_MP3;
	}
	else if (song_name_len > 4 \
			 && (song[song_name_len - 1] == 'v' || song[song_name_len - 2] == 'V') \
			 && (song[song_name_len - 2] == 'a' || song[song_name_len - 2] == 'A') \
			 && (song[song_name_len - 3] == 'w' || song[song_name_len - 3] == 'W') \
			 && song[song_name_len - 4] == '.') {
		return AUDIO_TYPE_WAV;
	}

	return AUDIO_TYPE_NONE;
}

int app_audio::play(const char* song, double volume) {
	int ret = -1;
	m_playing_volume = volume;

	audio_type_e st_type = get_audio_type(song);
	switch (st_type) {
	case AUDIO_TYPE_MP3: {
		AUDIO_DBG("AUDIO_TYPE_MP3\n");
		play_mp3(song);
		ret = 0;
	}
		break;

	case AUDIO_TYPE_WAV: {
		AUDIO_DBG("AUDIO_TYPE_WAV\n");
		play_wav(song);
		ret = 0;
	}

	default:
		break;
	}

	return ret;
}

int app_audio::play_wav(const char* song) {
	if (m_pthread_player_state == THREAD_PLAYER_STATE_UNINT) {
		FATAL("app_audio", 0x10);
		return -3;
	}

	m_is_called_play = true;

	if (!m_playing_song_name) {
		m_playing_song_name = new string(song);
	}
	else {
		m_playing_song_name->assign(song);
	}

	if (m_pthread_player_state == THREAD_PLAYER_STATE_PLAYING) {
		/* stop request */
		set_pthread_player_state(THREAD_PLAYER_STATE_STOP);

		/* waiting player idle */
		while (get_pthread_player_state() != THREAD_PLAYER_STATE_IDLE);

		/* stop player handler */
		play_stop();
	}

	if (m_pthread_player_state == THREAD_PLAYER_STATE_IDLE) {
		uint32_t song_data_len;
		get_wav_song_format(m_playing_song_name->c_str(), &m_ao_sample_format, &song_data_len);
		AUDIO_DBG("song_data_len: %d\n", song_data_len);
		AUDIO_DBG("song: %s\n", m_playing_song_name->c_str());
		AUDIO_DBG("volume: %f\n", m_playing_volume);
		AUDIO_DBG("format.bits		= %d\n" , m_ao_sample_format.bits);
		AUDIO_DBG("format.rate		= %d\n" , m_ao_sample_format.rate);
		AUDIO_DBG("format.channels		= %d\n" , m_ao_sample_format.channels);
		AUDIO_DBG("format.byte_format	= %d\n" , m_ao_sample_format.byte_format);

		m_stream_buffer_size = 4096;
		m_stream_buffer = (unsigned char*)malloc(m_stream_buffer_size * sizeof(unsigned char));

		if (m_hw_id >= 0) {
			if (m_ao_sample_format.rate != 44100) { /* Convert Rate */
				if (m_ao_sample_format.channels == 1) { /* Mono */
					m_ao_option.value = (char*)hw_card_table[m_hw_id].MonoConvertCard.c_str();
				}
				else {
					m_ao_option.value = (char*)hw_card_table[m_hw_id].ConvertCard.c_str();
				}
			}
			else {
				if (m_ao_sample_format.channels == 1) { /* Mono */
					m_ao_option.value = (char*)hw_card_table[m_hw_id].MonononConvertCard.c_str();
				}
				else {
					m_ao_option.value = (char*)hw_card_table[m_hw_id].nonConvertCard.c_str();
				}
			}
			AUDIO_DBG("Play On Hw Card: %s !!!\n", m_ao_option.value);
		}

		m_ao_device = ao_open_live(m_ao_driver_id, &m_ao_sample_format, &m_ao_option);

		m_wav_pfile = fopen(m_playing_song_name->c_str(), "rb");

		if (m_ao_device && m_wav_pfile) {
			set_volume(m_playing_volume);
			pthread_create(&m_pthread_player, NULL, thread_wav_player_entry, this);

			set_pthread_player_state(THREAD_PLAYER_STATE_PLAYING);
			return 1;
		}
		else {
			play_stop();
			return -1;
		}
	}

	return -2; // Unkown state

	return 0;
}

int app_audio::play_mp3(const char* song) {
	if (m_pthread_player_state == THREAD_PLAYER_STATE_UNINT) {
		FATAL("app_audio", 0x10);
		return -3;
	}

	m_is_called_play = true;

	if (!m_playing_song_name) {
		m_playing_song_name = new string(song);
	}
	else {
		m_playing_song_name->assign(song);
	}

	if (m_pthread_player_state == THREAD_PLAYER_STATE_PLAYING) {
		/* stop request */
		set_pthread_player_state(THREAD_PLAYER_STATE_STOP);

		/* waiting player idle */
		while (get_pthread_player_state() != THREAD_PLAYER_STATE_IDLE);

		/* stop player handler */
		play_stop();
	}

	if (m_pthread_player_state == THREAD_PLAYER_STATE_IDLE) {
		m_mpg123_handle = mpg123_new(NULL, &m_error);

		mpg123_open(m_mpg123_handle, song);
		mpg123_getformat(m_mpg123_handle, &m_info_rate, &m_info_channels, &m_info_encoding);

		m_ao_sample_format.bits = mpg123_encsize(m_info_encoding) * 8;
		m_ao_sample_format.rate = m_info_rate;
		m_ao_sample_format.channels = m_info_channels;
		m_ao_sample_format.byte_format = AO_FMT_NATIVE;
		m_ao_sample_format.matrix = 0;

		m_stream_buffer_size = mpg123_outblock(m_mpg123_handle);
		m_stream_buffer = (unsigned char*)malloc(m_stream_buffer_size * sizeof(unsigned char));

		AUDIO_DBG("m_stream_buffer_size: %d \n", (int)m_stream_buffer_size);
		AUDIO_DBG("song: %s\n", m_playing_song_name->c_str());
		AUDIO_DBG("volume: %f\n", m_playing_volume);
		AUDIO_DBG("format.bits		= %d\n" , m_ao_sample_format.bits);
		AUDIO_DBG("format.rate		= %d\n" , m_ao_sample_format.rate);
		AUDIO_DBG("format.channels		= %d\n" , m_ao_sample_format.channels);
		AUDIO_DBG("format.byte_format	= %d\n" , m_ao_sample_format.byte_format);

		if (m_hw_id >= 0) {
			if (m_ao_sample_format.rate != 44100) { /* Convert Rate */
				if (m_ao_sample_format.channels == 1) { /* Mono */
					m_ao_option.value = (char*)hw_card_table[m_hw_id].MonoConvertCard.c_str();
				}
				else {
					m_ao_option.value = (char*)hw_card_table[m_hw_id].ConvertCard.c_str();
				}
			}
			else {
				if (m_ao_sample_format.channels == 1) { /* Mono */
					m_ao_option.value = (char*)hw_card_table[m_hw_id].MonononConvertCard.c_str();
				}
				else {
					m_ao_option.value = (char*)hw_card_table[m_hw_id].nonConvertCard.c_str();
				}
			}
			AUDIO_DBG("Play On Hw Card: %s !!!\n", m_ao_option.value);
		}

		m_ao_device = ao_open_live(m_ao_driver_id, &m_ao_sample_format, &m_ao_option);

		if (m_ao_device) {
			set_volume(m_playing_volume);
			mpg123_open(m_mpg123_handle, m_playing_song_name->c_str());

			pthread_create(&m_pthread_player, NULL, thread_mp3_player_entry, this);

			set_pthread_player_state(THREAD_PLAYER_STATE_PLAYING);
			return 1;
		}
		else {
			play_stop();
			return -1;
		}
	}

	return -2; // Unkown state
}

void app_audio::play_stop() {
	if (m_stream_buffer) {
		free(m_stream_buffer);
		m_stream_buffer = NULL;
	}

	if (m_ao_device) {
		ao_close(m_ao_device);
		m_ao_device = NULL;
	}

	if (m_mpg123_handle) {
		mpg123_close(m_mpg123_handle);
		mpg123_delete(m_mpg123_handle);
		m_mpg123_handle = NULL;
	}

	if (m_wav_pfile) {
		fclose(m_wav_pfile);
		m_wav_pfile = NULL;
	}
}

void app_audio::stop() {
	if (m_pthread_player_state != THREAD_PLAYER_STATE_UNINT) {

		m_is_called_play = false;

		if (m_pthread_player_state != THREAD_PLAYER_STATE_IDLE) {
			/* stop request */
			set_pthread_player_state(THREAD_PLAYER_STATE_STOP);

			/* waiting player idle */
			while (get_pthread_player_state() != THREAD_PLAYER_STATE_IDLE);
		}

		play_stop();
	}
	else {
		FATAL("app_audio", 0x13);
	}
}

int app_audio::set_volume(double volume) {
	if (m_mpg123_handle) {
		m_playing_volume = volume;
		mpg123_volume(m_mpg123_handle, m_playing_volume);
		return 1;
	}
	else {
		m_playing_volume = volume;
		alsa_set_volume(m_playing_volume);
	}
	return -1;
}

double app_audio::get_volume() {
	return m_playing_volume;
}

int app_audio::is_called_play() {
	if (m_is_called_play) return 1;
	return 0;
}

void app_audio::mp3_player_handler() {
	int s_status;
	while (1) {
		s_status = mpg123_read(m_mpg123_handle, m_stream_buffer, m_stream_buffer_size, &m_stream_done);

		if (s_status == MPG123_OK) {
			ao_play(m_ao_device, (char*)m_stream_buffer, (unsigned int)m_stream_done);
		}
		else if (s_status == MPG123_DONE) {
			mpg123_close(m_mpg123_handle);
			mpg123_open(m_mpg123_handle, m_playing_song_name->c_str());
		}

		if (get_pthread_player_state() == THREAD_PLAYER_STATE_STOP) {
			set_pthread_player_state(THREAD_PLAYER_STATE_IDLE);
			pthread_exit(NULL);
		}

		usleep(1000);
	}
}

void app_audio::wav_player_handler() {
	AUDIO_DBG("wav_player_handler()\n");
	uint32_t elements_read = 0;
	while (1) {
		elements_read = fread(m_stream_buffer, sizeof(uint8_t), m_stream_buffer_size, m_wav_pfile);

		ao_play(m_ao_device, (char*)m_stream_buffer, (unsigned int)elements_read);
		if (elements_read < m_stream_buffer_size) {
			fclose(m_wav_pfile);
			m_wav_pfile = fopen(m_playing_song_name->c_str(), "rb");
		}

		if (get_pthread_player_state() == THREAD_PLAYER_STATE_STOP) {
			set_pthread_player_state(THREAD_PLAYER_STATE_IDLE);
			pthread_exit(NULL);
		}
	}
}

void app_audio::set_pthread_player_state(int state) {
	pthread_mutex_lock(&m_pthread_mutex_player);
	m_pthread_player_state = state;
	pthread_mutex_unlock(&m_pthread_mutex_player);

	switch(m_pthread_player_state) {
	case THREAD_PLAYER_STATE_UNINT: {
		AUDIO_DBG("app_audio::set_pthread_player_state( %s )\n", "THREAD_PLAYER_STATE_UNINT");
	}
		break;

	case THREAD_PLAYER_STATE_IDLE: {
		AUDIO_DBG("app_audio::set_pthread_player_state( %s )\n", "THREAD_PLAYER_STATE_IDLE");
	}
		break;

	case THREAD_PLAYER_STATE_PLAYING: {
		AUDIO_DBG("app_audio::set_pthread_player_state( %s )\n", "THREAD_PLAYER_STATE_PLAYING");
	}
		break;

	case THREAD_PLAYER_STATE_STOP: {
		AUDIO_DBG("app_audio::set_pthread_player_state( %s )\n", "THREAD_PLAYER_STATE_STOP");
	}
		break;
	}
}

int app_audio::get_pthread_player_state() {
	int ret;
	pthread_mutex_lock(&m_pthread_mutex_player);
	ret = m_pthread_player_state;
	pthread_mutex_unlock(&m_pthread_mutex_player);
	return ret;
}
