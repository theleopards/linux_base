#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>

#include "if_rf24.h"

#include "app.h"
#include "app_if.h"
#include "app_data.h"
#include "app_dbg.h"
#include "app_audio.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_debug.h"

#include "link.h"
#include "link_sig.h"

#define UnitTest(fmt, ...)	__LOG__(fmt, "UnitTest", ##__VA_ARGS__)

q_msg_t gw_task_debug_mailbox;


using namespace std;


void fopen_s(FILE ** file_pointer, const char * fname, const char * mode)
{
	*file_pointer = fopen(fname, mode);
}


void* gw_task_debug_entry(void*) {
	ak_msg_t* msg = AK_MSG_NULL;

	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_debug_entry\n");

	timer_set(GW_TASK_DEBUG_ID, GW_DEBUG_3, 2000, TIMER_ONE_SHOT);
	timer_set(GW_TASK_DEBUG_ID, GW_DEBUG_13, 4000, TIMER_ONE_SHOT);

	while (1) {
		/* get messge */
		msg = ak_msg_rev(GW_TASK_DEBUG_ID);

		switch (msg->header->sig) {

		case GW_DEBUG_1: {
			APP_DBG_SIG("GW_DEBUG_1\n");
			uint32_t data_len = get_data_len_dynamic_msg(msg);
			APP_DBG_SIG("data_len: %d\n", data_len);

			uint8_t* rev_data = (uint8_t*)malloc(data_len);
			get_data_dynamic_msg(msg, rev_data, data_len);

			//			APP_DBG_SIG("rev_data:");
			//			for (uint32_t i = 0; i < data_len; i++) {
			//				RAW_DBG(" %02X", *(rev_data + i));
			//			}
			//			RAW_DBG("\n");
		}
			break;

		case GW_DEBUG_2: {
			UnitTest("GW_DEBUG_2\n");
			uint8_t test_buf[256];
			for (int i = 0; i < 256; i++) {
				test_buf[i] = i;
			}

			ak_msg_t* s_msg = get_dynamic_msg();
			set_if_des_type(s_msg, IF_TYPE_UART_AC);
			set_if_src_type(s_msg, IF_TYPE_UART_GW);
			set_if_des_task_id(s_msg, AC_TASK_DBG_ID);
			set_if_sig(s_msg, AC_DBG_TEST_3);
			set_if_data_dynamic_msg(s_msg, test_buf, 256);

			set_msg_sig(s_msg, GW_IF_DYNAMIC_MSG_OUT);
			task_post(GW_TASK_IF_ID, s_msg);
		}
			break;

		case GW_DEBUG_3: {
			APP_DBG_SIG("GW_DEBUG_3\n");
			uint8_t test_buf[64];
			for (int i = 0; i < 64; i++) {
				test_buf[i] = 0xAA;
			}

			ak_msg_t* s_msg = get_common_msg();
			set_if_des_type(s_msg, IF_TYPE_UART_GW);
			set_if_src_type(s_msg, IF_TYPE_UART_AC);
			set_if_des_task_id(s_msg, AC_TASK_DBG_ID);
			set_if_sig(s_msg, AC_DBG_TEST_2);
			set_if_data_common_msg(s_msg, test_buf, 64);

			set_msg_sig(s_msg, GW_IF_COMMON_MSG_OUT);
			task_post(GW_TASK_IF_ID, s_msg);
		}
			break;

		case GW_DEBUG_4: {
			APP_DBG_SIG("GW_DEBUG_4\n");
			static int GW_DEBUG_4_counter = 0;
			if (GW_DEBUG_4_counter++ > 100) {
				GW_DEBUG_4_counter = 0;
				APP_DBG_SIG("GW_DEBUG_4\n");
			}
			timer_set(GW_TASK_DEBUG_ID, GW_DEBUG_4, 10, TIMER_ONE_SHOT);
		}
			break;

		case GW_DEBUG_5: {
			APP_DBG_SIG("GW_DEBUG_5\n");
			static int GW_DEBUG_5_counter = 0;
			if (GW_DEBUG_5_counter++ > 1000) {
				GW_DEBUG_5_counter = 0;
				APP_DBG_SIG("GW_DEBUG_5\n");
			}
			timer_set(GW_TASK_DEBUG_ID, GW_DEBUG_7, 1, TIMER_ONE_SHOT);
		}
			break;

		case GW_DEBUG_6: {
			APP_DBG_SIG("GW_DEBUG_6\n");
			uint32_t data_len = get_data_len_dynamic_msg(msg);
			static unsigned long long  byte_counter = 0;
			byte_counter += data_len;
			UnitTest("link stream : %llu\n", byte_counter);
			//			APP_DBG_SIG("data_len: %d\n", data_len);
			//			uint8_t* rev_data = (uint8_t*)malloc(data_len);
			//			get_data_dynamic_msg(msg, rev_data, data_len);
			//			RAW_DBG("rev_data:");
			//			for (uint32_t i = 0; i < data_len; i++) {
			//				RAW_DBG(" %02X", *(rev_data + i));
			//			}
			//			RAW_DBG("\n");
			//			free(rev_data);

			timer_set(GW_TASK_DEBUG_ID, GW_DEBUG_12, 100, TIMER_ONE_SHOT);
			task_post_pure_msg(GW_TASK_DEBUG_ID, GW_DEBUG_3);
		}
			break;

		case GW_DEBUG_7: {
			APP_DBG_SIG("GW_DEBUG_7\n");
			static int GW_DEBUG_7_counter = 0;
			if (GW_DEBUG_7_counter++ > 1000) {
				GW_DEBUG_7_counter = 0;
				APP_DBG_SIG("GW_DEBUG_7\n");
			}
			task_post_pure_msg(GW_TASK_SYS_ID, GW_SYS_WATCH_DOG_DBG_4);
		}
			break;

		case GW_DEBUG_8: {
			APP_DBG_SIG("GW_DEBUG_8\n");
			static int GW_DEBUG_8_counter = 0;
			if (GW_DEBUG_8_counter++ > 1000) {
				GW_DEBUG_8_counter = 0;
				APP_DBG_SIG("GW_DEBUG_8\n");
			}
			task_post_pure_msg(GW_TASK_SYS_ID, GW_SYS_WATCH_DOG_DBG_3);
		}
			break;

		case GW_DEBUG_9: {
			APP_DBG_SIG("GW_DEBUG_9\n");
			static int GW_DEBUG_9_counter = 0;
			if (GW_DEBUG_9_counter++ > 1000) {
				GW_DEBUG_9_counter = 0;
				APP_DBG_SIG("GW_DEBUG_9\n");
			}
			task_post_pure_msg(GW_TASK_SYS_ID, GW_SYS_WATCH_DOG_DBG_2);
		}
			break;

		case GW_DEBUG_10: {
			APP_DBG_SIG("GW_DEBUG_10\n");
			static int GW_DEBUG_10_counter = 0;
			if (GW_DEBUG_10_counter++ > 1000) {
				GW_DEBUG_10_counter = 0;
				APP_DBG_SIG("GW_DEBUG_10\n");
			}
			timer_set(GW_TASK_SYS_ID, GW_SYS_WATCH_DOG_REPORT_REQ, 1, TIMER_ONE_SHOT);
		}
			break;

		case GW_DEBUG_11: {
			APP_DBG_SIG("GW_DEBUG_11\n");
			static int GW_DEBUG_11_counter = 0;
			if (GW_DEBUG_11_counter++ > 1000) {
				GW_DEBUG_11_counter = 0;
				APP_DBG_SIG("GW_DEBUG_11\n");
			}
			timer_set(GW_TASK_DEBUG_ID, GW_DEBUG_11, 2, TIMER_ONE_SHOT);
		}
			break;

		case GW_DEBUG_12: {
			APP_DBG_SIG("GW_DEBUG_12\n");
			//			ak_msg_t* s_msg = get_pure_msg();
			//			set_if_src_type(s_msg, IF_TYPE_UART_GW);
			//			set_if_des_type(s_msg, IF_TYPE_UART_AC);
			//			set_if_src_type(s_msg, 0x80);
			//			set_if_des_task_id(s_msg, AC_TASK_SYSTEM_ID);
			//			set_if_sig(s_msg, SYSTEM_AK_FLASH_UPDATE_REQ);
			//			set_msg_sig(s_msg, GW_IF_PURE_MSG_OUT);
			//			task_post(GW_TASK_IF_ID, s_msg);
		}
			break;

		case GW_DEBUG_13: {
			APP_DBG_SIG("GW_DEBUG_13\n");
			APP_PRINT("[shell_fw] update slave app request\n");
			gateway_fw_dev_update_req_t gateway_fw_dev_update_req;
			memset(&gateway_fw_dev_update_req, 0, sizeof(gateway_fw_dev_update_req_t));
			strcpy(gateway_fw_dev_update_req.dev_bin_path, "/home/thannt/workspace/projects/thannt/arm_cortex_m3_base_source/application/build_arm_cortex_m3_base_application_stm32l/arm_cortex_m3_base_application.bin");

			gateway_fw_dev_update_req.type_update   = TYPE_UPDATE_TARTGET_APP;
			gateway_fw_dev_update_req.source_if_type = IF_TYPE_UART_GW;
			gateway_fw_dev_update_req.target_task_id = AC_TASK_FW_ID;
			gateway_fw_dev_update_req.target_if_type = IF_TYPE_UART_AC;

			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, GW_FW_OTA_REQ);
			set_data_dynamic_msg(s_msg, (uint8_t*)&gateway_fw_dev_update_req, sizeof(gateway_fw_dev_update_req_t));
			set_msg_src_task_id(s_msg, GW_TASK_CONSOLE_ID);
			task_post(GW_TASK_FW_ID, s_msg);
		}
			break;

		default:
			break;
		}

		/* free message */
		ak_msg_free(msg);
	}

	return (void*)0;
}
