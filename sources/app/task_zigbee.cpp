#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>

#include "ak.h"

#include "sys_dbg.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"

#include "zb_zcl.h"
#include "zb_znp.h"

//https://github.com/athombv/homey/issues/2165
//https://github.com/Frans-Willem/AqaraHub/blob/master/documentation/devices/lumi.sensor_ht.md
//#define DBG_ZB_FRAME
//#define ZIGBE_SERIAL_DEV_PATH "/dev/ttyUSB0"
#define ZIGBE_SERIAL_DEV_PATH "/dev/ttyACM0"
//#define ZIGBE_SERIAL_DEV_PATH "/dev/ttyACM1"

q_msg_t gw_task_zigbee_mailbox;

uint16_t control_switch_address;
zb_znp zigbee_network;

static int zigbee_serial_fd;
static int zigbee_serial_open_dev(const char* devpath);
static pthread_t zigbe_serial_rx_thread;
static void* zigbe_serial_rx_thread_handler(void*);

typedef struct {
	devStates_t state;
	const char* state_name;
} devStatesString_t;

static devStatesString_t devStatesString[] = {
	{ DEV_HOLD,               "Initialized - not started automatically" },
	{ DEV_INIT,               "Initialized - not connected to anything" },
	{ DEV_NWK_DISC,           "Discovering PAN's to join" },
	{ DEV_NWK_JOINING,        "Joining a PAN" },
	{ DEV_NWK_REJOIN,         "ReJoining a PAN, only for end devices" },
	{ DEV_END_DEVICE_UNAUTH,  "Joined but not yet authenticated by trust center" },
	{ DEV_END_DEVICE,         "Started as device after authentication" },
	{ DEV_ROUTER,             "Device joined, authenticated and is a router" },
	{ DEV_COORD_STARTING,     "Started as Zigbee Coordinator" },
	{ DEV_ZB_COORD,           "Started as Zigbee Coordinator" },
	{ DEV_NWK_ORPHAN,          "Device has lost information about its parent.." },
};

int zb_znp::zigbee_message_handler(zigbee_msg_t& zigbee_msg) {
	/* zigbee start debug message */
#if defined (DBG_ZB_FRAME)
	printf("[ZB msg] len: %d cmd0: %x cmd1: %x \n", zigbee_msg.len, zigbee_msg.cmd0, zigbee_msg.cmd1);
	printf(" data: ");
	for (int i = 0; i < zigbee_msg.len; i++) {
		printf("%x ", (uint8_t)zigbee_msg.data[i]);
	}
	printf("\n");
#endif
	/* zigbee stop debug message */

	uint16_t zigbee_cmd = BUILD_UINT16(zigbee_msg.cmd1, zigbee_msg.cmd0);

	switch(zigbee_cmd) {
	case UTIL_GET_DEVICE_INFO_RESPONSE: {
		printf("UTIL_GET_DEVICE_INFO_RESPONSE\n");
		zb_get_device_info_t* zb_get_device_info = (zb_get_device_info_t*)zigbee_msg.data;
		printf("%s --> 0 1 byte Device state – See 4.5.52\n", devStatesString[zb_get_device_info->device_state].state_name);
		for (int i = 0; i < 8; i++) {
			printf("%02X ", ((uint8_t*)&zb_get_device_info->device_ieee_address)[i]);
		}
		printf("-->1 8 bytes Device IEEE address\n");
		printf("%04X --> 2 2 bytes Device short address\n", zb_get_device_info->device_short_address);
		printf("%04X --> 3 2 bytes Short address of the parent device\n", zb_get_device_info->parent_short_address);
		for (int i = 0; i < 8; i++) {
			printf("%02X ", ((uint8_t*)&zb_get_device_info->parent_ieee_address)[i]);
		}
		printf("--> 4 8 bytes IEEE address of the parent device\n");
		printf("%d --> 5 1 byte Channel on which the ZigBee network is operating\n", zb_get_device_info->operating_channel);
		printf("%04X --> 6 2 bytes PAN ID of the ZigBee network\n", zb_get_device_info->PAN_id);
		for (int i = 0; i < 8; i++) {
			printf("%02X ", ((uint8_t*)&zb_get_device_info->extended_PAN_id)[i]);
		}
		printf("--> 7 8 bytes Extended PAN Id of the ZigBee network\n");
	}
		break;

	case ZDO_MGMT_LEAVE_REQ: {
		printf("ZDO_MGMT_LEAVE_REQ\n");
	}
		break;

	case ZB_RECEIVE_DATA_INDICATION: {
		printf("ZB_RECEIVE_DATA_INDICATION\n");
	}
		break;

	case AF_DATA_REQUEST_IND: {
		printf("AF_DATA_REQUEST_IND\n");
		uint8_t* status = (uint8_t*)zigbee_msg.data;
		APP_DBG("\tstatus: %02x\n", *status);
	}

	case AF_DATA_CONFIRM: {
		APP_DBG("AF_DATA_CONFIRM\n");
		afDataConfirm_t* afDataConfirm = (afDataConfirm_t*)zigbee_msg.data;
		APP_DBG("\tstatus: %02x\n", afDataConfirm->status);
		APP_DBG("\tendpoint: %02x\n", afDataConfirm->endpoint);
		APP_DBG("\ttransID: %02x\n", afDataConfirm->transID);
	}
		break;

	case AF_INCOMING_MSG: {
		afIncomingMSGPacket_t* st_af_incoming_msg = (afIncomingMSGPacket_t*)zigbee_msg.data;

		zclProcMsgStatus_t zclProcMsgStatus = zcl_ProcessMessageMSG(st_af_incoming_msg);
		switch (zclProcMsgStatus) {
		case ZCL_PROC_SUCCESS: {
			printf("[zcl_ProcessMessageMSG] Message was processed\n");
		}
			break;

		case ZCL_PROC_INVALID: {
			printf("[zcl_ProcessMessageMSG] Format or parameter was wrong\n");
		}
			break;

		case ZCL_PROC_EP_NOT_FOUND: {
			printf("[zcl_ProcessMessageMSG] Endpoint descriptor not found\n");
		}
			break;

		case ZCL_PROC_NOT_OPERATIONAL: {
			printf("[zcl_ProcessMessageMSG] Can't respond to this command\n");
		}
			break;

		case ZCL_PROC_INTERPAN_FOUNDATION_CMD: {
			printf("[zcl_ProcessMessageMSG] INTER-PAN and Foundation Command (not allowed)\n");
		}
			break;

		case ZCL_PROC_NOT_SECURE: {
			printf("[zcl_ProcessMessageMSG] Security was required but the message is not secure\n");
		}
			break;

		case ZCL_PROC_MANUFACTURER_SPECIFIC: {
			printf("[zcl_ProcessMessageMSG] Manufacturer Specific command - not handled\n");
		}
			break;

		case ZCL_PROC_MANUFACTURER_SPECIFIC_DR: {
			printf("[zcl_ProcessMessageMSG] Manufacturer Specific command - not handled, but default response sent\n");
		}
			break;

		case ZCL_PROC_NOT_HANDLED: {
			printf("[zcl_ProcessMessageMSG] No default response was sent and the message was not handled\n");
		}
			break;

		case ZCL_PROC_NOT_HANDLED_DR: {
			printf("[zcl_ProcessMessageMSG] default response was sent and the message was not handled\n");
		}

		default:
			printf("zclProcMsgStatus: %d\n", zclProcMsgStatus);
			break;
		}

#if defined (DBG_ZB_FRAME)
		printf("AF_INCOMING_MSG:\n");
		printf("\tgroup_id: %04x \n", st_af_incoming_msg->group_id);
		printf("\tcluster_id: %04x\n", st_af_incoming_msg->cluster_id);
		printf("\tsrc_addr: %04x\n", st_af_incoming_msg->src_addr);
		printf("\tsrc_endpoint: %x\n", st_af_incoming_msg->src_endpoint);
		printf("\tdst_endpoint: %x\n", st_af_incoming_msg->dst_endpoint);
		printf("\twas_broadcast: %x\n", st_af_incoming_msg->was_broadcast);
		printf("\tlink_quality: %x\n", st_af_incoming_msg->link_quality);
		printf("\tsecurity_use: %x\n", st_af_incoming_msg->security_use);
		printf("\ttime_stamp: %08x\n", st_af_incoming_msg->time_stamp);
		printf("\ttrans_seq_num: %x\n", st_af_incoming_msg->trans_seq_num);
		printf("\tlen: %d\n", st_af_incoming_msg->len);
		printf("\tdata: ");
		for (int i = 0 ; i < st_af_incoming_msg->len ; i++) {
			printf("%02x ", st_af_incoming_msg->payload[i]);
		}
		printf("\n");
#endif
	}
		break;

	case ZDO_MGMT_LEAVE_RSP: {
		APP_DBG("ZDO_MGMT_LEAVE_RSP\n");
	}
		break;

	case ZDO_MGMT_PERMIT_JOIN_RSP: {
		APP_DBG("ZDO_MGMT_PERMIT_JOIN_RSP\n");
		ZdoMgmtPermitJoinRspInd_t* ZdoMgmtPermitJoinRspInd = (ZdoMgmtPermitJoinRspInd_t*)zigbee_msg.data;
		APP_DBG("\tsrcaddr: %08x \n", ZdoMgmtPermitJoinRspInd->srcaddr);
		APP_DBG("\tstatus: %d \n", ZdoMgmtPermitJoinRspInd->status);
	}
		break;

	case ZDO_TC_DEV_IND: {
		APP_DBG("ZDO_TC_DEV_IND\n");
	}
		break;

	case ZDO_END_DEVICE_ANNCE_IND: {
		APP_DBG("ZDO_END_DEVICE_ANNCE_IND\n");
		ZDO_DeviceAnnce_t* ZDO_DeviceAnnce = (ZDO_DeviceAnnce_t*)zigbee_msg.data;
		APP_DBG("\tSrcAddr: %04x \n", ZDO_DeviceAnnce->SrcAddr);
		APP_DBG("\tnwkAddr: %04x \n", ZDO_DeviceAnnce->nwkAddr);
		APP_DBG("\textAddr: ");
		for (int i = 0 ; i < Z_EXTADDR_LEN ; i++) {
			printf("%02x ", ZDO_DeviceAnnce->extAddr[i]);
		}
		APP_DBG("\n");
		/***
		 * Specifies the MAC capabilities of the device.
		 * Bit: 0 – Alternate PAN Coordinator
		 * 1 – Device type: 1- ZigBee Router; 0 – End Device
		 * 2 – Power Source: 1 Main powered
		 * 3 – Receiver on when idle
		 * 4 – Reserved
		 * 5 – Reserved
		 * 6 – Security capability
		 * 7 – Reserved
		 */
		APP_DBG("\tcapabilities: %d\n", ZDO_DeviceAnnce->capabilities);
	}
		break;

	default: {
		printf("zigbee_cmd: 0x%02X\n", zigbee_cmd);
	}
		break;
	}

	return 0;
}

void* gw_task_zigbee_entry(void*) {
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_zigbee_entry\n");

	if (zigbee_serial_open_dev(ZIGBE_SERIAL_DEV_PATH) >= 0) {
		APP_DBG("zigbee_serial_open_dev OK !\n");
		zigbee_network.set_znp_stream_fd(zigbee_serial_fd);

		if (zigbee_network.start_coordinator(1) == 0) {
			APP_DBG("start_coordinator successfully\n");
			pthread_create(&zigbe_serial_rx_thread, NULL, zigbe_serial_rx_thread_handler, NULL);
		}
		else {
			APP_DBG("start_coordinator error\n");
		}
	}
	else {
		APP_DBG("zigbee_serial_open_dev ERR !\n");
	}

	sleep(1);

	ak_msg_t* msg;

	while (1) {
		/* get messge */
		msg = ak_msg_rev(GW_TASK_ZIGBEE_ID);

		/* handler message */
		switch (msg->header->sig) {

		case GW_ZIGBEE_INIT: {
			APP_DBG_SIG("GW_ZIGBEE_INIT\n");
		}
			break;

		case GW_ZIGBEE_FORCE_START_COODINATOR: {
			APP_DBG_SIG("GW_ZIGBEE_FORCE_START_COODINATOR\n");
			if (zigbee_network.start_coordinator(1) == 0) {
				APP_DBG("force start_coordinator successfully\n");
			}
			else {
				APP_DBG("force start_coordinator error\n");
			}
		}
			break;

		case GW_ZIGBEE_START_COODINATOR: {
			APP_DBG_SIG("GW_ZIGBEE_START_COODINATOR\n");
			if (zigbee_network.start_coordinator(0) == 0) {
				APP_DBG("start_coordinator successfully\n");
			}
			else {
				APP_DBG("start_coordinator error\n");
			}
		}
			break;

		case GW_ZIGBEE_FORCE_START_ROUTER: {
			APP_DBG_SIG("GW_ZIGBEE_FORCE_START_ROUTER\n");
			if (zigbee_network.start_router(1) == 0) {
				APP_DBG("force start_router successfully\n");
			}
			else {
				APP_DBG("force start_router error\n");
			}
		}
			break;

		case GW_ZIGBEE_START_ROUTER: {
			APP_DBG_SIG("GW_ZIGBEE_START_ROUTER\n");
			if (zigbee_network.start_router(0) == 0) {
				APP_DBG("start_router successfully\n");
			}
			else {
				APP_DBG("start_router error\n");
			}
		}
			break;

		case GW_ZIGBEE_PERMIT_JOINING_REQ: {
			APP_DBG_SIG("GW_ZIGBEE_PERMIT_JOINING_REQ\n");
			zigbee_network.set_permit_joining_req(ALL_ROUTER_AND_COORDINATOR, 60, 1);
		}
			break;

		case GW_ZIGBEE_BDB_START_COMMISSIONING: {
			APP_DBG_SIG("GW_ZIGBEE_BDB_START_COMMISSIONING\n");
			uint8_t znpCmdExeResult;
			znpCmdExeResult = zigbee_network.bdb_start_commissioning(COMMISSIONING_MODE_STEERING, 1, 0);		// 0x02 is Network Steering
			if (znpCmdExeResult != ZNP_RET_OK) {
				APP_DBG_SIG("[Start Coodinator] ERR: COMMISSIONING_MODE_STEERING\n");
			}
			else {
				APP_DBG_SIG("[Start Coodinator] OK: COMMISSIONING_MODE_STEERING\n");
			}
		}
			break;

		case GW_ZIGBEE_ZCL_CMD_HANDLER: {
			APP_DBG_SIG("GW_ZIGBEE_ZCL_CMD_HANDLER\n");

			zclOutgoingMsg_t* pOutgoingMsg;
			get_data_common_msg(msg, (uint8_t*)&pOutgoingMsg, sizeof(pOutgoingMsg));

			APP_DBG("[pOutgoingMsg] short_addr = 0x%04x\n", pOutgoingMsg->short_addr);
			APP_DBG("[pOutgoingMsg] cluster_id = 0x%04x\n", pOutgoingMsg->cluster_id);
			APP_DBG("[pOutgoingMsg] group_id = 0x%04x\n", pOutgoingMsg->group_id);
			APP_DBG("[pOutgoingMsg] cmd = 0x%02x\n", pOutgoingMsg->cmd);
			APP_DBG("[pOutgoingMsg] attrID = 0x%04x\n", pOutgoingMsg->attrID);
			APP_DBG("[pOutgoingMsg] dataType = 0x%02x\n", pOutgoingMsg->dataType);
			APP_DBG("[pOutgoingMsg] dataLen = %d\n", pOutgoingMsg->dataLen);
			APP_DBG("[pOutgoingMsg] data: ");
			for (int i = 0 ; i < pOutgoingMsg->dataLen ; i++) {
				printf("%02x ", pOutgoingMsg->data[i]);
			}
			printf("\n");

			// Handle data message incoming.
			switch (pOutgoingMsg->cluster_id) {
			case ZCL_CLUSTER_ID_MS_RELATIVE_HUMIDITY: {
				printf("ZCL_CLUSTER_ID_MS_RELATIVE_HUMIDITY\n");
				uint16_t retHum = (uint16_t)(pOutgoingMsg->data[pOutgoingMsg->dataLen] + pOutgoingMsg->data[pOutgoingMsg->dataLen - 1] * 256);
				// Ví dụ: retHum = 6789, thì giá trị trả về là 67,89 %
				printf("HUMIDITY: %0.2f\n", (float)(retHum / 100));
			}
				break;

			case ZCL_CLUSTER_ID_MS_TEMPERATURE_MEASUREMENT: {
				printf("ZCL_CLUSTER_ID_MS_TEMPERATURE_MEASUREMENT\n");
				uint16_t retTemp = (uint16_t)(pOutgoingMsg->data[pOutgoingMsg->dataLen] + pOutgoingMsg->data[pOutgoingMsg->dataLen - 1] * 256);
				// Ví dụ: retTemp = 2723, thì giá trị trả về là 27,23 *C
				printf("TEMPERATURE: %0.2f\n", (float)(retTemp / 100));
			}
				break;

			case ZCL_CLUSTER_ID_GEN_ON_OFF: {
				printf("ZCL_CLUSTER_ID_GEN_ON_OFF\n");
				uint8_t retOnOff = pOutgoingMsg->data[0];
				printf("ON_OFF: %d\n", retOnOff);
			}
				break;

			case ZCL_CLUSTER_ID_GEN_BASIC: {
				printf("ZCL_CLUSTER_ID_GEN_BASIC\n");
				switch (pOutgoingMsg->attrID) {
				case ATTRID_XIAOMI_SENS_STATUS_REPORT: {
					if (pOutgoingMsg->dataLen == sizeof(xiaomi_sens_status_report_t) &&
							pOutgoingMsg->data[1] == 1) {

						uint8_t battery_level = 0;
						xiaomi_sens_status_report_t xiaomi_sens_status_report;
						memcpy(&xiaomi_sens_status_report, pOutgoingMsg->data, sizeof(xiaomi_sens_status_report_t));
						printf("<SENS> BATTERY VOLTAGE: %d\n", xiaomi_sens_status_report.battery_value);

						//https://devzone.nordicsemi.com/f/nordic-q-a/28101/how-to-calculate-battery-voltage-into-percentage-for-aa-2-batteries-without-fluctuations
						if (xiaomi_sens_status_report.battery_value >= 3000) {
							xiaomi_sens_status_report.battery_value  = 100;
						}
						else if (xiaomi_sens_status_report.battery_value > 2900) {
							battery_level = 100 - ((3000 - xiaomi_sens_status_report.battery_value ) * 60) / 100;
						}
						else if (xiaomi_sens_status_report.battery_value > 2740) {
							battery_level = 60 - ((2900 - xiaomi_sens_status_report.battery_value) * 40) / 150;
						}
						else {
							battery_level = 0;
						}

						printf("<SENS> BATTERY PERCENT: %d\n", battery_level);
						printf("<SENS> TEMPERATURE: %d\n", xiaomi_sens_status_report.temperatemure_value / 100);
						printf("<SENS> HUMIDITY: %d\n", xiaomi_sens_status_report.humidity_value / 100);
					}
				}
					break;

				default:
					break;
				}
			}
				break;

			default:
				break;
			}

			// free message.
			if (pOutgoingMsg) {
				free(pOutgoingMsg);
			}

			if (pOutgoingMsg->data) {
				free(pOutgoingMsg->data);
			}
		}
			break;

		case GW_ZIGBEE_GET_MAC_ADDR_REQ: {
			APP_DBG_SIG("GW_ZIGBEE_GET_MAC_ADDR_REQ\n");
		}
			break;

		case GW_ZIGBEE_TEST: {
			APP_DBG_SIG("GW_ZIGBEE_TEST\n");
			zigbee_network.util_get_device_info();
		}
			break;

		default:
			break;
		}

		/* free message */
		ak_msg_free(msg);
	}

	return (void*)0;
}

int zigbee_serial_open_dev(const char* devpath) {
	struct termios options;
	APP_DBG("[ZIGBEE][zigbee_serial_open_dev] devpath: %s\n", devpath);

	zigbee_serial_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);

	APP_DBG("zigbee_serial_fd: %d\n", zigbee_serial_fd);

	if (zigbee_serial_fd < 0) {
		return zigbee_serial_fd;
	}
	else {
		fcntl(zigbee_serial_fd, F_SETFL, 0);

		/* get current status */
		tcgetattr(zigbee_serial_fd, &options);

		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);

		/* No parity (8N1) */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag     &=  ~CRTSCTS;

		cfmakeraw(&options);

		tcflush(zigbee_serial_fd, TCIFLUSH);
		if (tcsetattr (zigbee_serial_fd, TCSANOW, &options) != 0) {
			SYS_DBG("error in tcsetattr()\n");
		}
	}
	return 0;
}

void* zigbe_serial_rx_thread_handler(void*) {
	APP_DBG("[ZIGBEE] zigbe_serial_rx_thread_handler entry successed!\n");
	while(1){
		zigbee_network.update();
		usleep(1000);
	}
	return (void*)0;
}
