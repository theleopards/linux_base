-include sources/app/mqtt/Makefile.mk
-include sources/app/interfaces/Makefile.mk
-include sources/app/ArduinoZigBee/Makefile.mk

CXXFLAGS	+= -I./sources/app

VPATH += sources/app

OBJ += $(OBJ_DIR)/app.o
OBJ += $(OBJ_DIR)/app_data.o
OBJ += $(OBJ_DIR)/app_config.o
OBJ += $(OBJ_DIR)/app_audio.o
OBJ += $(OBJ_DIR)/shell.o

OBJ += $(OBJ_DIR)/task_list.o
OBJ += $(OBJ_DIR)/task_console.o
OBJ += $(OBJ_DIR)/task_if.o
OBJ += $(OBJ_DIR)/task_debug.o
OBJ += $(OBJ_DIR)/task_sensor.o
OBJ += $(OBJ_DIR)/task_fw.o
OBJ += $(OBJ_DIR)/task_sys.o
OBJ += $(OBJ_DIR)/task_zigbee.o
