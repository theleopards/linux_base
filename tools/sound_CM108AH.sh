#!/bin/sh

NUMBER=1

CARDS=$(wc -l < /proc/asound/cards)

echo "Number of line ${CARDS}"

for i in `seq 3 $CARDS`
do

CARD_NUMBER_LIST=$(sed -e "$i!d" -e 's/ //g' /proc/asound/cards | cut -c1-1)

	if [ "$CARD_NUMBER_LIST" = "1" ]
	then
		CARD_NAME=$(sed -e "$i!d" -e 's/ //g' -e 's/].*//' -r -e 's/^.{2}//' /proc/asound/cards)
		echo "Fanning get CARD$CARD_NUMBER_LIST"
		echo "CARD NAME: $CARD_NAME"
		GET_USB_BUS_POS=`expr $i + $NUMBER`
		echo $GET_USB_BUS_POS
		USB_BUS=$(sed -e "$GET_USB_BUS_POS!d" -e 's/ //g' -e 's/\,[^\.]*$//' -r -e 's/^.{41}//' /proc/asound/cards)
		
		USB_BUS_INDEX=$(cat /proc/asound/card1/usbbus | cut -c3)
		echo $USB_BUS_INDEX	
		echo $USB_BUS
		SERIAL=$(cat "/sys/bus/usb/devices/$USB_BUS_INDEX$USB_BUS/serial" | cut -c12)

		echo $SERIAL

		touch "/root/SPEAKER$SERIAL"
		echo $CARD_NAME > "/root/SPEAKER$SERIAL"

	fi

	if [ "$CARD_NUMBER_LIST" = "2" ]
	then
		CARD_NAME=$(sed -e "$i!d" -e 's/ //g' -e 's/].*//' -r -e 's/^.{2}//' /proc/asound/cards)
		echo "Fanning get CARD$CARD_NUMBER_LIST"
		echo "CARD NAME: $CARD_NAME"
		GET_USB_BUS_POS=`expr $i + $NUMBER`
		echo $GET_USB_BUS_POS
		USB_BUS=$(sed -e "$GET_USB_BUS_POS!d" -e 's/ //g' -e 's/\,[^\.]*$//' -r -e 's/^.{41}//' /proc/asound/cards)
		USB_BUS_INDEX=$(cat /proc/asound/card1/usbbus | cut -c3)
                echo $USB_BUS_INDEX
                echo $USB_BUS      
		SERIAL=$(cat "/sys/bus/usb/devices/$USB_BUS_INDEX$USB_BUS/serial" | cut -c12)

                echo $SERIAL

                touch "/root/SPEAKER$SERIAL"
                echo $CARD_NAME > "/root/SPEAKER$SERIAL"

	fi

	if [ "$CARD_NUMBER_LIST" = "3" ]
	then
		CARD_NAME=$(sed -e "$i!d" -e 's/ //g' -e 's/].*//' -r -e 's/^.{2}//' /proc/asound/cards)
		echo "Fanning get CARD$CARD_NUMBER_LIST"
		echo "CARD NAME: $CARD_NAME"
		GET_USB_BUS_POS=`expr $i + $NUMBER`
		echo $GET_USB_BUS_POS
		USB_BUS=$(sed -e "$GET_USB_BUS_POS!d" -e 's/ //g' -e 's/\,[^\.]*$//' -r -e 's/^.{41}//' /proc/asound/cards)
		USB_BUS_INDEX=$(cat /proc/asound/card1/usbbus | cut -c3)
                echo $USB_BUS_INDEX
                echo $USB_BUS      
                SERIAL=$(cat "/sys/bus/usb/devices/$USB_BUS_INDEX$USB_BUS/serial" | cut -c12)

                echo $SERIAL

                touch "/root/SPEAKER$SERIAL"
                echo $CARD_NAME > "/root/SPEAKER$SERIAL"

	fi
	
	if [ "$CARD_NUMBER_LIST" = "4" ]
	then
		CARD_NAME=$(sed -e "$i!d" -e 's/ //g' -e 's/].*//' -r -e 's/^.{2}//' /proc/asound/cards)
		echo "Fanning get CARD$CARD_NUMBER_LIST"
		echo "CARD NAME: $CARD_NAME"
		GET_USB_BUS_POS=`expr $i + $NUMBER`
		echo $GET_USB_BUS_POS
		USB_BUS=$(sed -e "$GET_USB_BUS_POS!d" -e 's/ //g' -e 's/\,[^\.]*$//' -r -e 's/^.{41}//' /proc/asound/cards)
		USB_BUS_INDEX=$(cat /proc/asound/card1/usbbus | cut -c3)
                echo $USB_BUS_INDEX
                echo $USB_BUS      
                SERIAL=$(cat "/sys/bus/usb/devices/$USB_BUS_INDEX$USB_BUS/serial" | cut -c12)

                echo $SERIAL

                touch "/root/SPEAKER$SERIAL"
                echo $CARD_NAME > "/root/SPEAKER$SERIAL"


	fi

done
